use crate::config::Config;
use crate::nasa;

use serenity::{
    model::{channel::Message, gateway::Ready},
    prelude::*,
};

struct Handler {
    nasa_key: String,
}

impl EventHandler for Handler {
    // Set a handler for the `message` event - so that whenever a new message
    // is received - the closure (or function) passed will be called.
    //
    // Event handlers are dispatched through a threadpool, and so multiple
    // events can be dispatched simultaneously.
    fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "~ping" || msg.content == "!ping" {
            // Sending a message can fail, due to a network error, an
            // authentication error, or lack of permissions to post in the
            // channel, so log to stdout when some error happens, with a
            // description of it.
            if let Err(why) = msg.channel_id.say(&ctx.http, "Pong!") {
                println!("Error sending message: {:?}", why);
            }
        } else if msg.content == "~apod" {
            let apod = nasa::get_apod(&self.nasa_key)
                .unwrap_or_else(|err| {
                    println!("Error fetching from NASA: {:?}", err);
                    "Error fetching from NASA".into()
                });
            if let Err(why) = msg.channel_id.say(&ctx.http, &apod) {
                println!("Error sending message: {:?}", why);
            }
        } else if msg.content == "~help" {
            if let Err(why) = msg.channel_id.say(&ctx.http, "Commands: ~help, ~ping, ~apod") {
                println!("Error sending message: {:?}", why);
            }
        }
    }

    // Set a handler to be called on the `ready` event. This is called when a
    // shard is booted, and a READY payload is sent by Discord. This payload
    // contains data like the current user's guild Ids, current user data,
    // private channels, and more.
    //
    // In this case, just print what the current user's username is.
    fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}

pub fn run_bot(config: &Config) {
    // Configure the client with your Discord bot token in the environment.

    // Create a new instance of the Client, logging in as a bot. This will
    // automatically prepend your bot token with "Bot ", which is a requirement
    // by Discord for bot users.
    let mut client = Client::new(
        &config.discord.token,
        Handler {
            nasa_key: config.nasa.key.clone(),
        },
    )
    .expect("Err creating client");

    // Finally, start a single shard, and start listening to events.
    //
    // Shards will automatically attempt to reconnect, and will perform
    // exponential backoff until it reconnects.
    if let Err(why) = client.start() {
        println!("Client error: {:?}", why);
    }
}
