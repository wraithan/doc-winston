use std::fs::read_to_string;
use std::path::Path;

use serde_derive::Deserialize;
use toml;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Config {
    pub discord: DiscordConfig,
    pub nasa: NasaConfig,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DiscordConfig {
    pub client_id: String,
    pub secret: String,
    pub token: String,
    pub permissions: u32,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NasaConfig {
    pub key: String,
}

impl Config {
    pub fn from_file<P: AsRef<Path>>(path: P) -> Self {
        let config_data = read_to_string(path).expect("Couldn't read config file");
        toml::from_str(&config_data).expect("Couldn't parse config file")
    }
}
