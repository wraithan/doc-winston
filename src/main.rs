mod config;
mod discord;
mod nasa;

use clap::{App, Arg};
use config::Config;

fn main() {
    let matches = App::new("doc-winston")
        .version("1.0")
        .author("Wraithan")
        .about("Astronomy Pictures")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("debug")
                .short("d")
                .multiple(true)
                .help("Turn debugging information on"),
        )
        .get_matches();

    let config = if let Some(c) = matches.value_of("config") {
        println!("Value for config: {}", c);
        Config::from_file(c)
    } else {
        unimplemented!("fall back to environment variables");
    };

    println!(
        "https://discordapp.com/api/oauth2/authorize?client_id={}&scope=bot&permissions={}",
        config.discord.client_id, config.discord.permissions
    );
    discord::run_bot(&config);
}
