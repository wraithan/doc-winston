use reqwest;
use serde_derive::Deserialize;

#[derive(Deserialize, Debug)]
struct Apod {
    date: String,
    explanation: String,
    hdurl: String,
    media_type: String,
    service_version: String,
    title: String,
    url: String,
}

pub fn get_apod(key: &str) -> Result<String, reqwest::Error> {
    let url = format!("https://api.nasa.gov/planetary/apod?api_key={}", key);
    let mut response = reqwest::get(&url)?;
    let apod_data: Apod = response.json()?;
    let message = format!(
        "**Title:** {title}\n**Date:** {date}\n**Description:** {description}\n**Link:** {link}",
        date = apod_data.date,
        description = apod_data.explanation,
        title = apod_data.title,
        link = apod_data.url
    );
    Ok(message)
}
